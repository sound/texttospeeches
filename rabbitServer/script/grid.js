function generateGrid() {
	var grid = {};
	var currentX = 0;
	var currentY = 0;
	var w = $(window).width(), h = $(window).height();

	var squareWidth = w / 12;
	var squareHeight = h/9;
	
	var sounds = ["cafe_alberto","casino_lisboa", "hotel_diamantino", "restaurante_damasio", "restaurante_pedro", "supermercado_frutinha_boa", "universidade_lisboa"];
	
	
	
	for(var i = 0; i < 9; i++) {
			for(var j = 0; j < 12; j++) {
			
			document.body.innerHTML += "<div class='gridSquare touchable' style='left: " +  currentX+ "px; top: " + currentY + "px'></div>";
	
			currentX += squareWidth + 3;
			}
		
		currentX = 0;
		currentY += squareHeight + 3;
		
	}
	
	$.each( sounds, function( key, value ) {
		
		$($('.gridSquare')[Math.floor((Math.random()*107)+1)]).attr("name", value).text(value);
		
	});
	
	$('.gridSquare').each(function() {
		var $this = $(this);
		$this.width(squareWidth);
		$this.height(squareHeight);
	});

}