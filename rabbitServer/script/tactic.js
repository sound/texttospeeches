var tree;
var touches;
var objects;
var registered_objects;
var object_tree;
var table_object_tree;
var sounds = ["bombaGasolina","cafealberto","cafeMiranda","campoFutebol","campoPequeno","casinolisboa","catedralSJoao","cinemaMundial","colombo","escolaOlivais","estacaoMetro","estadioLuz","estadioPedreia","garrafeiraNacional","gelatariaCampos","hoteldiamantino","igrejaPared","jardimEstrela","jardimTilias","largoCarmo","lojaRoupa","mercadoRibeira","mosteiroAlcobaça","museuArteAntiga","museuSabores","palacioBolsa","paragemAutocarro","parqueEstacionamento","parqueTecnológico","peixariaFerreira","praiaCarcavelos","restaurantedamasio","restaurantepedro","rotunda","supermercadofrutinhaboa","tabernaTrindade","talhoFernandes","tascaEsquina","teatroAberto","universidadelisboa"];	
function refreshTracking() {
	
	tree = new RTree();

	//collect divs for event triggering
	$('.touchable, .object-aware, .movable').each(function() {
		var $this = $(this);
		tree.insert({x: $this.offset().left, y: $this.offset().top, w: $this.width(), h: $this.height()}, $this);
	});
}

function initTracking(dontDraw) {

	touches = {};// touches
	objects = {}; //objects
	registered_objects = []; //registered objects on the table
	
	tree = new RTree();
	object_tree = new RTree();
	table_object_tree = new RTree();
	
	
	//collect divs for event triggering
	$('.touchable, .object-aware, .movable').each(function() {
		var $this = $(this);
		tree.insert({x: $this.offset().left, y: $this.offset().top, w: $this.width(), h: $this.height()}, $this);
	});

	tuio.object_add(function(data) {

		objects[data.fid] = {
			x : data.x * w,
			y: data.y * h,
			angle: data.angle,
            inside: null
		};

		
		for(id in registered_objects) {
			var ball = registered_objects[id];
			
			//object_tree.remove({x: ball.x - 100, y: ball.y - 100, w: 200, h:200}, ball);
			registered_objects.splice(id,1);
			
		}
		
		var $this = objects[data.fid];
		table_object_tree.insert({x: $this.x - 100, y: $this.y - 100, w:200, h:200}, $this);
		
		$('*').trigger("object_added", [{id: data.fid, x: data.x * w, y: data.y * h, angle: data.angle}]);7
		
	});
	
	tuio.object_update(function(data){
	
   objects[data.fid] = {
			x : data.x * w,
			y: data.y * h,
			angle: data.angle,
            inside: objects[data.fid].inside
		};
	    
		$('*').trigger("object_updated", [{id: data.fid, x: data.x * w, y: data.y * h, angle: data.angle}]);

		var hasObject = false;
        
		tree.search({x: data.x * w, y: data.y * h, w: 15, h: 15}).filter(function($div){return $div.hasClass("object-aware")}).map(function($el){
        
        	if(objects[data.fid].inside == null) {
            	objects[data.fid].inside = $el;
        		$el.trigger('object.added', [{id: data.fid, x: data.x * w, y: data.y * h, angle: data.angle}]);
            }
			else if (objects[data.fid].inside == $el)
				$el.trigger('object.updated', [{id: data.fid, x: data.x * w, y: data.y * h, angle: data.angle}]); 
            hasObject = true;    
		});
        
        if(!hasObject && objects[data.fid].inside != null) {
        	$(objects[data.fid].inside).trigger('object.removed', [{id: data.fid, x: data.x * w, y: data.y * h, angle: data.angle}]);  
            objects[data.fid].inside = null;
        } 
	});
	
	tuio.object_remove(function(data) {
		$('*').trigger('object_removed', [{id: data.fid, x: data.x * w, y: data.y * h, angle: data.angle}]);
        if(objects[data.fid].inside != null)
        	$(objects[data.fid].inside).trigger('object.removed', [{id: data.fid, x: data.x * w, y: data.y * h, angle: data.angle}]);  
        
		
		
		if (isTrackingAboveTheTable) {
		
			var hand = getClosestHand(data.x * w, data.y * h);
			
			if(validateHandHolding(hand)) {
			
				registered_objects[data.fid] = {
					fid: data.fid,
					x: data.x * w,
					y: data.y * h,
					handHolding: hand
				};
			}
			
			//object_tree.insert({x: data.x * w - 100, y: data.y * h - 100, w: 200, h: 200}, registered_objects[data.fid]);
					
			//remove from table_object_tree
			var $this = objects[data.fid];
			table_object_tree.remove({x: $this.x - 100, y: $this.y - 100, w: 200, h: 200}, $this);
		}
		
		
		delete objects[data.fid];
	});
	
	tuio.cursor_add(function(data) {
	
		var isTouch = true; 
		
		table_object_tree.search({x: data.x * w, y: data.y * h, w: 15, h: 15}).map(function($el){			
			isTouch = false;
		});
		
		//ignore if it's not a touch
		if(isTouch) {
			touches[data.sid] = {
				x: data.x * w,
				y: data.y * h,
				hits: [],
				div: null,
				initX: 0,
				initY: 0
			};
			tree.search({x: data.x * w, y: data.y * h, w: 15, h: 15}).filter(function($div){return $div.hasClass("touchable")}).map(function($el) {
				touches[data.sid].hits.push($el);
				$el.trigger('touch.press', [{x: data.x * w, y: data.y * h, id: data.fid}]);
				touches[data.sid].div = $el;
			});
			
			tree.search({x: data.x * w, y: data.y * h, w: 15, h: 15}).filter(function($div){return $div.hasClass("movable")}).map(function($el) {
				touches[data.sid].div = $el;
				touches[data.sid].initX = data.x * w - $($el).position().left;
				touches[data.sid].initY = data.y * h - $($el).position().top;          
			}); 
		}  		
	});
    
	
	tuio.cursor_update(function(data) {
	
		if(touches[data.sid] != null) { 
			touches[data.sid].x = data.x * w;
			touches[data.sid].y = data.y * h;
			
			tree.search({x: data.x * w, y: data.y * h, w: 1, h: 1}).filter(function($div){return $div.hasClass("touchable")}).map(function($el) {
				//touches[data.sid].hits.push($el);
				$el.trigger('touch.update', [{x: data.x * w, y: data.y * h, id: data.fid}]);
			});

			tree.search({x: data.x * w, y: data.y * h, w: 15, h: 15}).filter(function($div){return $div.hasClass("movable")}).map(function($el){
				if(touches[data.sid].div == $el) {
					var x = data.x * w - touches[data.sid].initX;
					var y = data.y * h - touches[data.sid].initY;
				
					$el.trigger('touch.moved', [{x: x, y: y}]);
					$($el).css('left', x);
					$($el).css('top', y);
					tree.remove({x: $el.offset().left, y: $el.offset().top, w: $el.width(), h: $el.height()}, $el);
					tree.insert({x: x, y: y, w: $el.width(), h: $el.height()}, $el);
				}
			});
		}
	});
	
	tuio.cursor_remove(function(data) {
		if(touches[data.sid] != null) { 
			touches[data.sid].hits.map(function($el) {
				$el.trigger('touch.release', [{x: data.x * w, y: data.y * h, id: data.fid}]);
			});
			delete touches[data.sid];
		}
	});
	
	// that's how we draw them
	var updateStarted = false;
	var redrawTouch = function drawBalls() {
		ctx.clearRect(0, 0, w, h);
		
		for (id in touches) {
			var ball = touches[id];
			ctx.beginPath();
				ctx.arc(ball.x, ball.y, 30, 0, 2 * Math.PI, true);
				ctx.fillStyle = "rgba(0, 0, 255, 0.2)";
				ctx.fill();
				ctx.lineWidth = 5.0;
				ctx.strokeStyle = "rgba(0, 0, 0, 0.7)";
			ctx.stroke();
		}
        
		
        for (id in objects) {
			var ball = objects[id];
			ctx.beginPath();
				ctx.arc(ball.x, ball.y, 70 + 40* ball.angle, 0, 2 * Math.PI, true);
				ctx.fillStyle = "rgba(150, 150, 150, 1.0)";
				ctx.fill();
				ctx.lineWidth = 5.0;
				ctx.strokeStyle = "rgba(0, 0, 0, 0.7)";
			ctx.stroke();
		}
		
		for (id in registered_objects) {
			var ball = registered_objects[id];
			ctx.beginPath();
				ctx.arc(ball.x, ball.y, 150, 0, 2 * Math.PI, true);
				ctx.fillStyle = "rgba(150, 150, 150, 1.0)";
				ctx.fill();
				ctx.lineWidth = 5.0;
				ctx.strokeStyle = "rgba(0, 0, 0, 0.7)";
			ctx.stroke();
		}		
		
	}
	
	// schedule redraw
	if (!dontDraw) {
		setInterval(function() {
			if (updateStarted) return;
			updateStarted = true;
			redrawTouch();
			updateStarted = false;
		}, 15);
	}	
	
	// setup the canvas
	var canvas = $('#canvas').get(0), ctx = canvas.getContext('2d');
	var w = $(window).width(), h = $(window).height();
	canvas.width = w;
	canvas.height = h;
	
	// start the thing!
	tuio.start();
	$('#__tuiojs_connector_npTuioClient').css('visibility', 'hidden');
    $('#canvas').css('position', 'absolute');
}

function getClosestHand(a,b) {
	var w = $(window).width(), h = $(window).height();
	
	var c = rightHandPosition.x * w;
	var d = rightHandPosition.z * h;
	var distanceRight = Math.sqrt(Math.pow(c-a,2) + Math.pow(d-b,2));
	
	var c2 = leftHandPosition.x * w;
	var d2 = leftHandPosition.z * h;
	var distanceLeft = Math.sqrt(Math.pow(c2-a,2) + Math.pow(d2-b,2));
	
	
	return distanceRight > distanceLeft ? "LEFT" : "RIGHT";
}

function getClosestFinger(a,b) {
	var w = $(window).width(), h = $(window).height();
	
	var fingers = getClosestHand(a,b) == "RIGHT" ? fingersRight : fingersLeft;
	
	var distance = 500000;
	var finger = 0;
	
	for(var i = 0; i < 5; i++) {
		var c = fingers[i].x * w;
		var d = fingers[i].z * h;
		
		var newDistance = Math.sqrt(Math.pow(c-a,2) + Math.pow(d-b,2));
		if(newDistance < distance) {
			distance = newDistance;
			finger = i;
		} 
	}	
	return finger;
	
}

function validateHandHolding(hand) {
	return hand =="RIGHT" && rightHandClosed == 1|| hand =="LEFT" && leftHandClosed == 1;
}

var rightHandClosed = 0;
var leftHandClosed = 0;
var leftHandPosition;
var rightHandPosition;

var isTrackingAboveTheTable = false;

var fingersLeft;
var fingersRight;

function initTrackingAboveTheTable(){

	isTrackingAboveTheTable = true;
	
	var canvas = $('#canvas').get(0), ctx = canvas.getContext('2d');
	var w = $(window).width(), h = $(window).height();
	
	
	MQ.queue("auto", {autoDelete : true}).bind("handInfo", "*.fingerPositions").callback(function(m) {
		
		
		
		fingersLeft = m.data.fingerTips[0];
		fingersRight = m.data.fingerTips[1];
		
		var canvas = $('#canvas').get(0), ctx = canvas.getContext('2d');
		var w = $(window).width(), h = $(window).height();
		
		/**
		for(var i = 0; i < 2; i++) {
			for(var j= 0; j < 5; j++){
			ctx.beginPath();
				ctx.arc(m.data.fingerTips[i][j].x * w, m.data.fingerTips[i][j].z * h, 25, 0, 2 * Math.PI, true);
				ctx.fillStyle = "rgba(150, 150, 150, 1.0)";
				ctx.fill();
				ctx.lineWidth = 5.0;
				ctx.strokeStyle = "rgba(0, 0, 0, 0.7)";
			ctx.stroke();
			}
		}
		*/
		//which hand is grabbing
		
		if(m.data.fingerTips[1][2].x * w- m.data.fingerTips[1][0].x * w < 100)
			rightHandClosed = 1;
		else rightHandClosed = 0;
			
		
		if(m.data.fingerTips[0][0].x * w - m.data.fingerTips[0][2].x * w < 100)
			leftHandClosed = 1;
		else leftHandClosed = 0;
		
	});
	
	
	MQ.queue("auto", {autoDelete : true}).bind("handInfo", "*.handPosition").callback(function(m) {
	
		var thisHand = m.data.hand;
		
		if(thisHand == "RIGHT") rightHandPosition = m.data.position;
		else if (thisHand == "LEFT") leftHandPosition = m.data.position;
		
		var w = $(window).width(), h = $(window).height();

		var handX = m.data.position.x * w, handY = m.data.position.z * h;
		
		for (id in registered_objects) {
		
			var object = registered_objects[id];
			if(object.handHolding == thisHand) { 
				if(thisHand =="RIGHT" && rightHandClosed == 0 && leftHandClosed == 1|| thisHand =="LEFT" && leftHandClosed == 0 && rightHandClosed == 1) {
					//aqui coloca-se o desaparecer do objecto e o trocar de mão (por agora fica o trocar de mão)
					
					thisHand = thisHand == "RIGHT" ? "LEFT" : "RIGHT";
				}

				registered_objects[object.fid] = {
						fid: object.fid,
						x: handX,
						y: handY,
						handHolding: thisHand
				};
				
				$('*').trigger('object_hovering', [{id: object.fid, x: handX, y: handY, hand: thisHand }]);

			}
		}		
	});

}