function initMessaging() {
	MQ.configure({
		host: 'localhost',
		element: 'amqp'
	});
	swfobject.switchOffAutoHideShow();
	swfobject.embedSWF('/amqp/amqp.swf', 'amqp', '1', '1', '9', '/amqp/expressInstall.swf', {}, {
		allowScriptAccess: 'always',
		wmode: 'transparent',
		scale: 'noscale',
		bgcolor: '#000000'
	}, {});
	MQ.on('connect', function() {
		$('.touchable').fadeTo('slow', 1);
		$('#status').text("Ready");
	});
}