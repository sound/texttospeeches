package sound_experiments;

import paulscode.sound.SoundSystemConfig;

public class AudioSource {
	
	private boolean priority;
	private boolean loop;
	private String sourceName;
	private String fileName;
	private float x;
	private float y;
	private float z;
	private float[] position;
	private int aModel;
	private float rFactor;

	public AudioSource(boolean priority, String sourceName, String fileName, boolean loop, float[] position, int aModel, float rFactor) {
		this.priority = priority;
		this.sourceName = sourceName;
		this.fileName = fileName;
		this.loop = loop;
		this.position = position;
		this.x=position[0];
		this.y=position[1];
		this.z=position[2];
		this.aModel = aModel;
		this.rFactor=rFactor;
		
	}
	
	public AudioSource(String sourceName, String fileName, float[] position) {
		this.priority = false;
		this.sourceName = sourceName;
		this.fileName = fileName;
		this.loop = false;
		this.position = position;
		this.x=position[0];
		this.y=position[1];
		this.z=position[2];
		
		this.aModel = SoundSystemConfig.ATTENUATION_ROLLOFF;
		//this.rFactor = 0.5f;
		this.rFactor = SoundSystemConfig.getDefaultRolloff();
	}

	public boolean isPriority() {
		return priority;
	}

	public void setPriority(boolean priority) {
		this.priority = priority;
	}

	public boolean isLoop() {
		return loop;
	}

	public void setLoop(boolean loop) {
		this.loop = loop;
	}

	public String getSourceName() {
		return sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public float getZ() {
		return z;
	}

	public void setZ(float z) {
		this.z = z;
	}

	public float[] getPosition() {
		return position;
	}

	public void setPosition(float[] position) {
		this.position = position;
		this.setX(position[0]);
		this.setY(position[1]);
		this.setZ(position[2]);
	}

	public int getaModel() {
		return aModel;
	}

	public void setaModel(int aModel) {
		this.aModel = aModel;
	}

	public float getrFactor() {
		return rFactor;
	}

	public void setrFactor(float rFactor) {
		this.rFactor = rFactor;
	}

}
