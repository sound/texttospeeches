package sound_experiments;

public class Utils {
	
	public static float[] getRightPosition(float distance)
	{
		return new float[] {distance, 0, 0};	
	}
	
	public static float[] getLeftPosition(float distance)
	{
		return new float[] {-distance, 0, 0};	
	}
	
	public static float[] getFrontPosition(float distance)
	{
		return new float[] {0, 0, -distance};	
	}
	
	public static float[] getBackPosition(float distance)
	{
		return new float[] {0, 0, distance};
	}
	public static float[] getPositionByAngle(float distance, float angle){
		if (angle == 90) return new float[] {0,0,-distance};
		
		float x = (float) Math.cos(Math.toRadians(angle)) * distance;
		if(angle <0) x = - x;
		float z = (float) Math.sin(Math.toRadians(angle)) * distance;
		if(angle <0) z = - z;
		
		System.out.print(x);
		System.out.print(-z);
		return new float[] {x,0,-z};
	}
	
}
