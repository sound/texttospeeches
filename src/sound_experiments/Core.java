/**
 * 
 */
package sound_experiments;

import paulscode.sound.SoundSystem;
import paulscode.sound.SoundSystemConfig;
import paulscode.sound.SoundSystemException;
import paulscode.sound.libraries.LibraryJavaSound;
import paulscode.sound.libraries.LibraryLWJGLOpenAL;
import paulscode.sound.Library;
import paulscode.sound.codecs.CodecWav;
import paulscode.sound.codecs.CodecJOgg;

/**
 * @author Jo�o
 *
 */
public class Core {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new Core();
	}
	
	public Core()
	{
		addPlugins();
		
			// Instantiate the SoundSystem:
		SoundSystem mySoundSystem = new SoundSystem();
		
		boolean priority = false;
		String sourcename = "Source 1";
		String filename = "som1.ogg";
		boolean loop = false;
		float x = 0;
		float y = 0;
		float z = 0;
		int aModel = SoundSystemConfig.ATTENUATION_ROLLOFF;
		float rFactor = SoundSystemConfig.getDefaultRolloff();
		mySoundSystem.newStreamingSource( priority, sourcename, "result_news1.wav", loop, -30, y, z, aModel, rFactor );
		mySoundSystem.newStreamingSource( priority, "Source 2", "result_news2.wav", loop, 5, y, z, aModel, rFactor );
		mySoundSystem.newStreamingSource( priority, "Source 3", "result_news3.wav", loop, x, y, -50, aModel, rFactor );
		mySoundSystem.play( "Source 1" );
		mySoundSystem.play( "Source 2" );
	//	mySoundSystem.play( "Source 3" );
		
		// Wait for 10 seconds:
		sleep(60);
		
		//mySoundSystem.setPosition( "Source 1", 0, 0, -100 );
		//sleep(50);
		
		
		// Shut down:
		mySoundSystem.cleanup();
	}
	
	public void sleep( long seconds )
	{
		try
		{
			Thread.sleep( 1000 * seconds );
		}
		catch( InterruptedException e )
		{}
	}
	
	public void addPlugins()
	{
		//It tries to use the first library and if it is not compatible, passes to the next one.
		//It is also possible to switchLibrary on the fly and obtain the currentLibrary

		
		try
		{
			SoundSystemConfig.addLibrary( LibraryLWJGLOpenAL.class );
		}
		catch( SoundSystemException e )
		{
			System.err.println( "error linking with the LibraryLWJGLOpenAL plug-in" );
		}
		
		try
		{
			SoundSystemConfig.addLibrary( LibraryJavaSound.class );
		}
		catch( SoundSystemException e )
		{
			System.err.println( "error linking with the LibraryJavaSound plug-in" );
		}
		
		try
		{
			SoundSystemConfig.setCodec( "wav", CodecWav.class );
		}
		catch( SoundSystemException e )
		{
			System.err.println("error linking with the CodecWav plug-in" );
		}
		
		try
		{
			SoundSystemConfig.setCodec( "ogg", CodecJOgg.class );
		}
		catch( SoundSystemException e )
		{
			System.err.println("error linking with the CodecJOgg plug-in" );
		}
	}

}
