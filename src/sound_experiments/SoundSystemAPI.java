package sound_experiments;

import java.util.ArrayList;

import paulscode.sound.SoundSystem;
import paulscode.sound.SoundSystemConfig;
import paulscode.sound.SoundSystemException;
import paulscode.sound.libraries.LibraryJavaSound;
import paulscode.sound.libraries.LibraryLWJGLOpenAL;
import paulscode.sound.codecs.CodecWav;
import paulscode.sound.codecs.CodecJOgg;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * This API uses the SoundSystem implemented by paulscode (www.paulscode.com) and
 * tries to simplify some of the actions that can be performed in the SoundSystem.
 * 
 * @author Joao Guerreiro
 * 
 */
public class SoundSystemAPI {

	/**
	 * @param args
	 */
	public SoundSystem mySoundSystem;
	public ArrayList<AudioSource> sourcesToPlay;
	Properties prop = new Properties();
	InputStream input = null;
	
	
	public static void main(String args[]){
		//new SoundSystemAPI();
		
		SoundSystemAPI soundSystemAPI = new SoundSystemAPI();
		
		/*soundSystemAPI.addSourceRight("cenas", "results_base/6.wav", 10);
		soundSystemAPI.playSource("cenas");
		soundSystemAPI.addSourceLeft("cenas2", "results_base/3.wav", 10);
		soundSystemAPI.playSource("cenas2");
		*/
		soundSystemAPI.addSourceFront("cenas3", "results_base/75.wav", 10);
		soundSystemAPI.playSource("cenas3");
		
	}
	
	
	/**
	 * SoundSystemAPI Constructor. Adds libraries and codecs
	 * and initializes our variables
	 * 
	 */
	public SoundSystemAPI()
	{
		this.addLibraries();
		this.addCodecs();
		this.mySoundSystem = new SoundSystem();
		this.sourcesToPlay = new ArrayList<AudioSource>();
		
		
		try {
			this.input = new FileInputStream("src/config.properties");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		// load a properties file
		try {
			this.prop.load(this.input);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	/**
	 * Adds a sound source to the Sound System, but does not start to play it.
	 * For long audio files, you should use addStreamSource (but there are limited Streaming sources - 4)
	 * 
	 * @param sourceName The name of the source
	 * @param fileName A .wav or .ogg filename
	 * @param position The position where the sound source should play [x,y,z]
	 */
	public void addSource(String sourceName, String fileName, float[] position)
	{
		AudioSource newSource = new AudioSource(sourceName, fileName, position);
		this.sourcesToPlay.add(newSource);
		mySoundSystem.newSource(newSource.isPriority(), newSource.getSourceName(),
				newSource.getFileName(), newSource.isLoop(), newSource.getX(), newSource.getY(),
				newSource.getZ(),newSource.getaModel(), newSource.getrFactor());
	}
	
	/**
	 * Adds a sound source to the Sound System, but does not start to play it. The location of the sound depends
	 * on the hand and finger that touched the table
	 * For long audio files, you should use addStreamSource (but there are limited Streaming sources - 4)
	 * 
	 * @param sourceName The name of the source
	 * @param fileName A .wav or .ogg filename
	 * @param hand Right or Left Hand
	 * @param finger The finger that started the event
	 * @param position The position where the sound source should play [x,y,z]
	 */
	public void addSourceByFinger(String sourceName, String fileName, String hand, String finger, float distance)
	{
		float angle = Float.parseFloat(prop.getProperty(hand+finger));
		float[] position = Utils.getPositionByAngle(distance, angle);

		AudioSource newSource = new AudioSource(sourceName, fileName, position);
		this.sourcesToPlay.add(newSource);
		mySoundSystem.newSource(newSource.isPriority(), newSource.getSourceName(),
				newSource.getFileName(), newSource.isLoop(), newSource.getX(), newSource.getY(),
				newSource.getZ(),newSource.getaModel(), newSource.getrFactor());
	}
	
	/**
	 * Adds a sound source to the Sound System and plays it. The location of the sound depends
	 * on the hand and finger that touched the table (config.properties contains the angle information)
	 * 
	 * @param sourceName The name of the source
	 * @param fileName A .wav or .ogg filename
	 * @param hand Right or Left Hand
	 * @param finger The finger that started the event
	 * @param position The position where the sound source should play [x,y,z]
	 */
	public void addSourceByFingerAndPlay(String sourceName, String fileName, String hand, String finger, float distance)
	{	
		float angle = Float.parseFloat(prop.getProperty(hand+finger));
		float[] position = Utils.getPositionByAngle(distance, angle);
		
		AudioSource newSource = new AudioSource(sourceName, fileName, position);
		this.sourcesToPlay.add(newSource);
		mySoundSystem.newSource(newSource.isPriority(), newSource.getSourceName(),
				newSource.getFileName(), newSource.isLoop(), newSource.getX(), newSource.getY(),
				newSource.getZ(),newSource.getaModel(), newSource.getrFactor());
		
		mySoundSystem.play(sourceName);
	}
	
	/**
	 * Adds a sound source to the Sound System, but does not start to play it. The location of the sound depends
	 * on the angle
	 * 
	 * @param sourceName The name of the source
	 * @param fileName A .wav or .ogg filename
	 * @param angle The angles should be from -90 to 90;
	 * @param side The side is important for the cases where the angle is 0
	 * @param distance The radius
	 */
	public void addSourceByAngle(String sourceName, String fileName, float angle, float distance)
	{
		if(angle == 0) {this.addSourceRight(sourceName, fileName, distance);return;}
		if(angle == 180){this.addSourceLeft(sourceName, fileName, distance); return;}


		float[] position = Utils.getPositionByAngle(distance, angle);

		AudioSource newSource = new AudioSource(sourceName, fileName, position);
		this.sourcesToPlay.add(newSource);
		mySoundSystem.newSource(newSource.isPriority(), newSource.getSourceName(),
				newSource.getFileName(), newSource.isLoop(), newSource.getX(), newSource.getY(),
				newSource.getZ(),newSource.getaModel(), newSource.getrFactor());
	}
	
	/**
	 * Adds a sound source to the Sound System and plays it. The location of the sound depends on the angle
	 * 
	 * @param sourceName The name of the source
	 * @param fileName A .wav or .ogg filename
	 * @param angle The angles should be from -90 to 90;
	 * @param side The side is important for the cases where the angle is 0
	 * @param distance The radius
	 */
	public void addSourceByAngleAndPlay(String sourceName, String fileName, float angle, float distance)
	{
		if(angle == 0) {this.addSourceRightAndPlay(sourceName, fileName, distance);return;}
		if(angle == 180){this.addSourceLeftAndPlay(sourceName, fileName, distance); return;}
		
		float[] position = Utils.getPositionByAngle(distance, angle);
		System.out.print(position);

		AudioSource newSource = new AudioSource(sourceName, fileName, position);
		this.sourcesToPlay.add(newSource);
		mySoundSystem.newSource(newSource.isPriority(), newSource.getSourceName(),
				newSource.getFileName(), newSource.isLoop(), newSource.getX(), newSource.getY(),
				newSource.getZ(),newSource.getaModel(), newSource.getrFactor());
		
		mySoundSystem.play(sourceName);
	}
	
	
	/**
	 * Adds a sound source to the Sound System and starts to play it.
	 * For long audio files, you should use addStreamSourceAndPlay
	 * (but there are limited Streaming sources - 4)
	 * 
	 * @param sourceName The name of the source
	 * @param fileName A .wav or .ogg filename
	 * @param position The position where the sound source should play [x,y,z]
	 */
	public void addSourceAndPlay(String sourceName, String fileName, float[] position)
	{
		AudioSource newSource = new AudioSource(sourceName, fileName, position);
		mySoundSystem.newSource(newSource.isPriority(), newSource.getSourceName(),
				newSource.getFileName(), newSource.isLoop(), newSource.getX(), newSource.getY(),
				newSource.getZ(),newSource.getaModel(), newSource.getrFactor());
		
		try{
			this.mySoundSystem.play(sourceName);
		}
		catch( Exception e )
		{
			System.err.println("Error playing the source "+ sourceName+". The streaming source may not exist");
		}
	}
	
	/**
	 * Adds a sound source to the Sound System and starts to play it.
	 * For long audio files, you should use addStreamSourceAndPlay
	 * (but there are limited Streaming sources - 4)
	 * 
	 * @param priority In case of limited availability of Sound channels, it states a source priority
	 * @param sourceName The name of the source
	 * @param fileName A .wav or .ogg filename
	 * @param loop 
	 * @param position he position where the sound source should play [x,y,z]
	 * @param aModel
	 * @param rFactor
	 */
	public void addSource(boolean priority, String sourceName, String fileName, boolean loop, float[] position, int aModel, float rFactor)
	{
		AudioSource newSource = new AudioSource(priority, sourceName, fileName, loop, position, aModel, rFactor);
		this.sourcesToPlay.add(newSource);
		mySoundSystem.newSource(newSource.isPriority(), newSource.getSourceName(),
				newSource.getFileName(), newSource.isLoop(), newSource.getX(), newSource.getY(),
				newSource.getZ(),newSource.getaModel(), newSource.getrFactor());
	}
	
	/**
	 * Adds a source to the right (Y and Z are 0)
	 * 
	 * @param sourceName The name of the source
	 * @param fileName A .wav or .ogg filename
	 * @param distance Corresponds to the value of X, as Y and Z are 0
	 */
	public void addSourceRight(String sourceName, String fileName, float distance)
	{
		float[] position = Utils.getRightPosition(distance);
		AudioSource newSource = new AudioSource(sourceName, fileName, position);
		this.sourcesToPlay.add(newSource);
		mySoundSystem.newSource(newSource.isPriority(), newSource.getSourceName(),
				newSource.getFileName(), newSource.isLoop(), newSource.getX(), newSource.getY(),
				newSource.getZ(),newSource.getaModel(), newSource.getrFactor());
	}
	
	/**
	 * Adds a source to the left (Y and Z are 0)
	 * 
	 * @param sourceName The name of the source
	 * @param fileName A .wav or .ogg filename
	 * @param distance Corresponds to the value of -X, as Y and Z are 0
	 */
	public void addSourceLeft(String sourceName, String fileName, float distance)
	{
		float[] position = Utils.getLeftPosition(distance);
		AudioSource newSource = new AudioSource(sourceName, fileName, position);
		this.sourcesToPlay.add(newSource);
		mySoundSystem.newSource(newSource.isPriority(), newSource.getSourceName(),
				newSource.getFileName(), newSource.isLoop(), newSource.getX(), newSource.getY(),
				newSource.getZ(),newSource.getaModel(), newSource.getrFactor());
	}
	
	/**
	 * 
	 * Adds a source to the front (Y and X are 0)
	 * 
	 * @param sourceName The name of the source
	 * @param fileName A .wav or .ogg filename
	 * @param distance Corresponds to the value of -Z, as Y and X are 0
	 */
	public void addSourceFront(String sourceName, String fileName, float distance)
	{
		float[] position = Utils.getFrontPosition(distance);
		AudioSource newSource = new AudioSource(sourceName, fileName, position);
		this.sourcesToPlay.add(newSource);
		mySoundSystem.newSource(newSource.isPriority(), newSource.getSourceName(),
				newSource.getFileName(), newSource.isLoop(), newSource.getX(), newSource.getY(),
				newSource.getZ(),newSource.getaModel(), newSource.getrFactor());
	}
	
	/**
	 * 
	 * Adds a source to the back (Y and X are 0)
	 * 
	 * @param sourceName The name of the source
	 * @param fileName A .wav or .ogg filename
	 * @param distance Corresponds to the value of Z, as Y and X are 0
	 */
	public void addSourceBack(String sourceName, String fileName, float distance)
	{
		float[] position = Utils.getBackPosition(distance);
		AudioSource newSource = new AudioSource(sourceName, fileName, position);
		this.sourcesToPlay.add(newSource);
		mySoundSystem.newSource(newSource.isPriority(), newSource.getSourceName(),
				newSource.getFileName(), newSource.isLoop(), newSource.getX(), newSource.getY(),
				newSource.getZ(),newSource.getaModel(), newSource.getrFactor());
	}
	
	/**
	 * Adds a source to the right and play it (Y and Z are 0)
	 * 
	 * @param sourceName The name of the source
	 * @param fileName A .wav or .ogg filename
	 * @param distance Corresponds to the value of X, as Y and Z are 0
	 */
	public void addSourceRightAndPlay(String sourceName, String fileName, float distance)
	{
		float[] position = Utils.getRightPosition(distance);
		AudioSource newSource = new AudioSource(sourceName, fileName, position);
		mySoundSystem.newSource(newSource.isPriority(), newSource.getSourceName(),
				newSource.getFileName(), newSource.isLoop(), newSource.getX(), newSource.getY(),
				newSource.getZ(),newSource.getaModel(), newSource.getrFactor());
		
		try{
			this.mySoundSystem.play(sourceName);
		}
		catch( Exception e )
		{
			System.err.println("Error playing the source "+ sourceName+". The streaming source may not exist");
		}
	}
	
	/**
	 * Adds a source to the left and plays it (Y and Z are 0)
	 * 
	 * @param sourceName The name of the source
	 * @param fileName A .wav or .ogg filename
	 * @param distance Corresponds to the value of -X, as Y and Z are 0
	 */
	public void addSourceLeftAndPlay(String sourceName, String fileName, float distance)
	{
		float[] position = Utils.getLeftPosition(distance);
		AudioSource newSource = new AudioSource(sourceName, fileName, position);
		mySoundSystem.newSource(newSource.isPriority(), newSource.getSourceName(),
				newSource.getFileName(), newSource.isLoop(), newSource.getX(), newSource.getY(),
				newSource.getZ(),newSource.getaModel(), newSource.getrFactor());
		
		try{
			this.mySoundSystem.play(sourceName);
		}
		catch( Exception e )
		{
			System.err.println("Error playing the source "+ sourceName+". The streaming source may not exist");
		}
	}
	
	/**
	 * 
	 * Adds a source to the front and plays it (Y and X are 0)
	 * 
	 * @param sourceName The name of the source
	 * @param fileName A .wav or .ogg filename
	 * @param distance Corresponds to the value of -Z, as Y and X are 0
	 */
	public void addSourceFrontAndPlay(String sourceName, String fileName, float distance)
	{
		float[] position = Utils.getFrontPosition(distance);
		AudioSource newSource = new AudioSource(sourceName, fileName, position);
		mySoundSystem.newSource(newSource.isPriority(), newSource.getSourceName(),
				newSource.getFileName(), newSource.isLoop(), newSource.getX(), newSource.getY(),
				newSource.getZ(),newSource.getaModel(), newSource.getrFactor());
		
		try{
			this.mySoundSystem.play(sourceName);
		}
		catch( Exception e )
		{
			System.err.println("Error playing the source "+ sourceName+". The streaming source may not exist");
		}
	}
	
	/**
	 * 
	 * Adds a source to the back and plays it (Y and X are 0)
	 * 
	 * @param sourceName The name of the source
	 * @param fileName A .wav or .ogg filename
	 * @param distance Corresponds to the value of Z, as Y and X are 0
	 */
	public void addSourceBackAndPlay(String sourceName, String fileName, float distance)
	{
		float[] position = Utils.getBackPosition(distance);
		AudioSource newSource = new AudioSource(sourceName, fileName, position);
		mySoundSystem.newSource(newSource.isPriority(), newSource.getSourceName(),
				newSource.getFileName(), newSource.isLoop(), newSource.getX(), newSource.getY(),
				newSource.getZ(),newSource.getaModel(), newSource.getrFactor());
		
		try{
			this.mySoundSystem.play(sourceName);
		}
		catch( Exception e )
		{
			System.err.println("Error playing the source "+ sourceName+". The streaming source may not exist");
		}
	}
	
	/**
	 * Adds a Streaming sound source to the Sound System, but does not start to play it.
	 * There are limited Streaming sources (4) and should be the longer ones.
	 * 
	 * @param sourceName The name of the source
	 * @param fileName A .wav or .ogg filename
	 * @param position The position where the sound source should play [x,y,z]
	 */
	public void addStreamSource(String sourceName, String fileName, float[] position)
	{
		AudioSource newSource = new AudioSource(sourceName, fileName, position);
		this.sourcesToPlay.add(newSource);
		mySoundSystem.newStreamingSource(newSource.isPriority(), newSource.getSourceName(),
				newSource.getFileName(), newSource.isLoop(), newSource.getX(), newSource.getY(),
				newSource.getZ(),newSource.getaModel(), newSource.getrFactor());
	}
	
	/**
	 * Adds a Streaming sound source to the Sound System and starts to play it.
	 * There are limited Streaming sources (4) and should be the longer ones.
	 * 
	 * @param sourceName The name of the source
	 * @param fileName A .wav or .ogg filename
	 * @param position The position where the sound source should play [x,y,z]
	 */
	public void addStreamSourceAndPlay(String sourceName, String fileName, float[] position)
	{
		AudioSource newSource = new AudioSource(sourceName, fileName, position);
		mySoundSystem.newStreamingSource(newSource.isPriority(), newSource.getSourceName(),
				newSource.getFileName(), newSource.isLoop(), newSource.getX(), newSource.getY(),
				newSource.getZ(),newSource.getaModel(), newSource.getrFactor());
		
		try{
			this.mySoundSystem.play(sourceName);
		}
		catch( Exception e )
		{
			System.err.println("Error playing the source "+ sourceName+". The streaming source may not exist");
		}
	}
	
	/**
	 * Adds a Streaming sound source to the Sound System and starts to play it.
	 * There are limited Streaming sources (4) and should be the longer ones
	 * 
	 * @param priority In case of limited availability of Sound channels, it states a source priority
	 * @param sourceName The name of the source
	 * @param fileName A .wav or .ogg filename
	 * @param loop 
	 * @param position he position where the sound source should play [x,y,z]
	 * @param aModel
	 * @param rFactor
	 */
	public void addStreamSource(boolean priority, String sourceName, String fileName, boolean loop, float[] position, int aModel, float rFactor)
	{
		AudioSource newSource = new AudioSource(priority, sourceName, fileName, loop, position, aModel, rFactor);
		this.sourcesToPlay.add(newSource);
		mySoundSystem.newStreamingSource(newSource.isPriority(), newSource.getSourceName(),
				newSource.getFileName(), newSource.isLoop(), newSource.getX(), newSource.getY(),
				newSource.getZ(),newSource.getaModel(), newSource.getrFactor());
	}
	
	/**
	 * Adds a Streaming source to the right (Y and Z are 0)
	 * 
	 * @param sourceName The name of the source
	 * @param fileName A .wav or .ogg filename
	 * @param distance Corresponds to the value of X, as Y and Z are 0
	 */
	public void addStreamSourceRight(String sourceName, String fileName, float distance)
	{
		float[] position = Utils.getRightPosition(distance);
		AudioSource newSource = new AudioSource(sourceName, fileName, position);
		this.sourcesToPlay.add(newSource);
		mySoundSystem.newStreamingSource(newSource.isPriority(), newSource.getSourceName(),
				newSource.getFileName(), newSource.isLoop(), newSource.getX(), newSource.getY(),
				newSource.getZ(),newSource.getaModel(), newSource.getrFactor());
	}
	
	/**
	 * Adds a Streaming source to the Left (Y and Z are 0)
	 * 
	 * @param sourceName The name of the source
	 * @param fileName A .wav or .ogg filename
	 * @param distance Corresponds to the value of -X, as Y and Z are 0
	 */
	public void addStreamSourceLeft(String sourceName, String fileName, float distance)
	{
		float[] position = Utils.getLeftPosition(distance);
		AudioSource newSource = new AudioSource(sourceName, fileName, position);
		this.sourcesToPlay.add(newSource);
		mySoundSystem.newStreamingSource(newSource.isPriority(), newSource.getSourceName(),
				newSource.getFileName(), newSource.isLoop(), newSource.getX(), newSource.getY(),
				newSource.getZ(),newSource.getaModel(), newSource.getrFactor());
	}
	
	/**
	 * Adds a Streaming source to the front (Y and X are 0)
	 * 
	 * @param sourceName The name of the source
	 * @param fileName A .wav or .ogg filename
	 * @param distance Corresponds to the value of -Z, as Y and X are 0
	 */
	public void addStreamSourceFront(String sourceName, String fileName, float distance)
	{
		float[] position = Utils.getFrontPosition(distance);
		AudioSource newSource = new AudioSource(sourceName, fileName, position);
		this.sourcesToPlay.add(newSource);
		mySoundSystem.newStreamingSource(newSource.isPriority(), newSource.getSourceName(),
				newSource.getFileName(), newSource.isLoop(), newSource.getX(), newSource.getY(),
				newSource.getZ(),newSource.getaModel(), newSource.getrFactor());
	}
	
	/**
	 * Adds a Streaming source to the BACK (Y and X are 0)
	 * 
	 * @param sourceName The name of the source
	 * @param fileName A .wav or .ogg filename
	 * @param distance Corresponds to the value of Z, as Y and X are 0
	 */
	public void addStreamSourceBack(String sourceName, String fileName, float distance)
	{
		float[] position = Utils.getBackPosition(distance);
		AudioSource newSource = new AudioSource(sourceName, fileName, position);
		this.sourcesToPlay.add(newSource);
		mySoundSystem.newStreamingSource(newSource.isPriority(), newSource.getSourceName(),
				newSource.getFileName(), newSource.isLoop(), newSource.getX(), newSource.getY(),
				newSource.getZ(),newSource.getaModel(), newSource.getrFactor());
	}
	
	/**
	 * Adds a Streaming source to the right and plays it (Y and Z are 0)
	 * 
	 * @param sourceName The name of the source
	 * @param fileName A .wav or .ogg filename
	 * @param distance Corresponds to the value of X, as Y and Z are 0
	 */
	public void addStreamSourceRightAndPlay(String sourceName, String fileName, float distance)
	{
		float[] position = Utils.getRightPosition(distance);
		AudioSource newSource = new AudioSource(sourceName, fileName, position);
		mySoundSystem.newStreamingSource(newSource.isPriority(), newSource.getSourceName(),
				newSource.getFileName(), newSource.isLoop(), newSource.getX(), newSource.getY(),
				newSource.getZ(),newSource.getaModel(), newSource.getrFactor());
		
		try{
			this.mySoundSystem.play(sourceName);
		}
		catch( Exception e )
		{
			System.err.println("Error playing the source "+ sourceName+". The streaming source may not exist");
		}
	}
	
	/**
	 * Adds a Streaming source to the Left and plays it (Y and Z are 0)
	 * 
	 * @param sourceName The name of the source
	 * @param fileName A .wav or .ogg filename
	 * @param distance Corresponds to the value of -X, as Y and Z are 0
	 */
	public void addStreamSourceLeftAndPlay(String sourceName, String fileName, float distance)
	{
		float[] position = Utils.getLeftPosition(distance);
		AudioSource newSource = new AudioSource(sourceName, fileName, position);
		mySoundSystem.newStreamingSource(newSource.isPriority(), newSource.getSourceName(),
				newSource.getFileName(), newSource.isLoop(), newSource.getX(), newSource.getY(),
				newSource.getZ(),newSource.getaModel(), newSource.getrFactor());
		
		try{
			this.mySoundSystem.play(sourceName);
		}
		catch( Exception e )
		{
			System.err.println("Error playing the source "+ sourceName+". The streaming source may not exist");
		}
	}
	
	/**
	 * Adds a Streaming source to the front and plays it (Y and X are 0)
	 * 
	 * @param sourceName The name of the source
	 * @param fileName A .wav or .ogg filename
	 * @param distance Corresponds to the value of -Z, as Y and X are 0
	 */
	public void addStreamSourceFrontAndPlay(String sourceName, String fileName, float distance)
	{
		float[] position = Utils.getFrontPosition(distance);
		AudioSource newSource = new AudioSource(sourceName, fileName, position);
		mySoundSystem.newStreamingSource(newSource.isPriority(), newSource.getSourceName(),
				newSource.getFileName(), newSource.isLoop(), newSource.getX(), newSource.getY(),
				newSource.getZ(),newSource.getaModel(), newSource.getrFactor());
		
		try{
			this.mySoundSystem.play(sourceName);
		}
		catch( Exception e )
		{
			System.err.println("Error playing the source "+ sourceName+". The streaming source may not exist");
		}
	}
	
	/**
	 * Adds a Streaming source to the BACK and plays it (Y and X are 0)
	 * 
	 * @param sourceName The name of the source
	 * @param fileName A .wav or .ogg filename
	 * @param distance Corresponds to the value of Z, as Y and X are 0
	 */
	public void addStreamSourceBackAndPlay(String sourceName, String fileName, float distance)
	{
		float[] position = Utils.getBackPosition(distance);
		AudioSource newSource = new AudioSource(sourceName, fileName, position);
		mySoundSystem.newStreamingSource(newSource.isPriority(), newSource.getSourceName(),
				newSource.getFileName(), newSource.isLoop(), newSource.getX(), newSource.getY(),
				newSource.getZ(),newSource.getaModel(), newSource.getrFactor());
		
		try{
			this.mySoundSystem.play(sourceName);
		}
		catch( Exception e )
		{
			System.err.println("Error playing the source "+ sourceName+". The streaming source may not exist");
		}
	}
	
	/**
	 * Plays the previously added source
	 * 
	 * @param sourceName The name of the source
	 */
	public void playSource(String sourceName){
		try{
			this.mySoundSystem.play(sourceName);
			sourcesToPlay.remove(sourceName);
		}
		catch( Exception e )
		{
			System.err.println("Error playing the source "+ sourceName+". The streaming source may not exist");
		}
	}
	
	/**
	 * Plays all the previously added sources, with a delay
	 * 
	 * @param delay In milliseconds
	 */
	public void playAllSources(long delay){
		for (AudioSource source : this.sourcesToPlay)
		{
			try{
				this.mySoundSystem.play(source.getSourceName());
				if(delay != 0) sleep(delay);
				
			}
			catch( Exception e )
			{
				System.err.println("Error playing the source "+ source.getSourceName()+". The streaming source may not exist");
			}
		}
	}
	
	/**
	 * Plays all sources previously added, simultaneously
	 * 
	 * 
	 */
	public void playAllSources(){
		for (AudioSource source : this.sourcesToPlay)
		{
			try{
				this.mySoundSystem.play(source.getSourceName());
			}
			catch( Exception e )
			{
				System.err.println("Error playing the source "+ source.getSourceName()+". The streaming source may not exist");
			}
		}
	}
	
	public void removeAllSources(){
		this.sourcesToPlay = new ArrayList<AudioSource>();
	}
	
	/**
	 * 
	 * Change the Position of a previously added source (it may be already playing or not)
	 * 
	 * @param sourceName The name of the source
	 * @param position The position where the sound source should play [x,y,z]
	 */
	public void setSourcePosition(String sourceName, float[] position)
	{
		this.mySoundSystem.setPosition(sourceName, position[0], position[1], position[2]);
	}
	
	/**
	 * Stops the Source, if it is playing
	 * 
	 * @param sourceName The name of the source
	 */
	public void stopExistentSource(String sourceName){
		try{
			this.mySoundSystem.stop(sourceName);
		}
		catch( Exception e )
		{
			System.err.println("Error stopping the source "+ sourceName+". The streaming source may not exist");
		}
	}
	
	/**
	 * 
	 * Removes the source, but does not stop it if it is playing
	 * 
	 * @param sourceName The name of the source
	 */
	public void removeSource(String sourceName){
		try{
			this.mySoundSystem.removeSource(sourceName);
			for(AudioSource source : sourcesToPlay){
				if(source.getSourceName().equals(sourceName))
					sourcesToPlay.remove(source);
			}
		}
		catch( Exception e )
		{
			System.err.println("Error removing the source "+ sourceName+". The streaming source may not exist");
		}
	}

	/**
	 * Cleanup the SoundSystem
	 */
	public void exitSoundSystem()
	{
		this.mySoundSystem.cleanup();
	}
	
	
	/**
	 * Add Libraries that the Sound System should use. The order is important to define priority.
	 * If the first one is not available it passes to the next one
	 */
	public void addLibraries()
	{
		//It tries to use the first library and if it is not compatible, passes to the next one.
		//It is also possible to switchLibrary on the fly and obtain the currentLibrary

		
		try
		{
			SoundSystemConfig.addLibrary( LibraryLWJGLOpenAL.class );
		}
		catch( SoundSystemException e )
		{
			System.err.println( "error linking with the LibraryLWJGLOpenAL plug-in" );
		}
		
		try
		{
			SoundSystemConfig.addLibrary( LibraryJavaSound.class );
		}
		catch( SoundSystemException e )
		{
			System.err.println( "error linking with the LibraryJavaSound plug-in" );
		}
	}
		
	/**
	 * Add the codecs that are needed to play the audio files
	 */
	public void addCodecs(){
		
		try
		{
			SoundSystemConfig.setCodec( "wav", CodecWav.class );
		}
		catch( SoundSystemException e )
		{
			System.err.println("error linking with the CodecWav plug-in" );
		}
		
		try
		{
			SoundSystemConfig.setCodec( "ogg", CodecJOgg.class );
		}
		catch( SoundSystemException e )
		{
			System.err.println("error linking with the CodecJOgg plug-in" );
		}
	}
	
	/**
	 * @param miliseconds
	 */
	public void sleep( long miliseconds )
	{
		try
		{
			Thread.sleep(miliseconds);
		}
		catch( InterruptedException e )
		{}
	}

}