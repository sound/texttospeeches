package sound_sockets;

import java.io.*;
import java.net.*;


public class clientExperiment {

	public static void main(String argv[]) throws Exception {

        Socket clientSocket = new Socket("localhost", 7999);
        PrintWriter outToServer = new PrintWriter(clientSocket.getOutputStream(), true);
        
        try{
    		BufferedReader br = 
                          new BufferedReader(new InputStreamReader(System.in));
     
    		String input;
     
    		while((input=br.readLine())!=null){
    			System.out.println(input);
    			if(input.equals("a")) outToServer.println("addSourceLeftAndPlay,source1,result_news1.wav,30");
    			else if(input.equals("b")) outToServer.println("addSourceLeftAndPlay,source2,result_news2.wav,15");
    			else if(input.equals("c")) outToServer.println("addSourceFrontAndPlay,source3,result_news3.wav,15");
    		}
     
    	}catch(IOException io){
    		io.printStackTrace();
    	}	
        /*
        outToServer.println("addSourceLeftAndPlay,source1,result_news1.wav,30");
        outToServer.println("addSourceLeftAndPlay,source2,result_news2.wav,15");
        Thread.sleep(3000);
        outToServer.println("stopExistentSource,source2");
        outToServer.println("removeSource,source2");
        Thread.sleep(2000);
        outToServer.println("addSourceRightAndPlay,source2,result_news2.wav,15");
        Thread.sleep(2000);*/
        //outToServer.println("addSourceFrontAndPlay,source3,result_news3.wav,15");
        //outToServer.println("setSourcePosition,source1,30,0,0");

        outToServer.close();
        clientSocket.close();
    }

}
