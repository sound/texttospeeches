package sound_sockets;

import sound_experiments.SoundSystemAPI;

import java.io.*;
import java.net.Socket;


public class ClientHandler implements Runnable {

	Socket socket;
	PrintStream out;
	
	SoundSystemAPI mySoundSystem;

	ClientHandler(Socket s, SoundSystemAPI soundSystem) {
		this.socket = s;
		this.mySoundSystem = soundSystem;
	}

	@Override
	public void run() {

		try {
			BufferedReader socketReader = 
					new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
			DataOutputStream outToClient = new DataOutputStream(this.socket.getOutputStream());
			
			String inputLine;
			String[] data;
			
			while ((inputLine = socketReader.readLine()) != null) 
            { 
             System.out.println ("Server: " + inputLine); 
             
             data = inputLine.split(",");
             System.out.println(data[0]);
             
             switch (data[0]) {
	             case "addSource": 
	            	 if (data.length == 6)
	            	 {
	            		 float[] position = {Float.parseFloat(data[3]), Float.parseFloat(data[4]), Float.parseFloat(data[5])};
	            		 this.mySoundSystem.addSource(data[1], data[2], position); 
	            	 }
	            	 else if (data.length == 10)
	            	 {
	            		 float[] position = {Float.parseFloat(data[5]), Float.parseFloat(data[6]), Float.parseFloat(data[7])};
	            		 this.mySoundSystem.addSource(Boolean.parseBoolean(data[1]), data[2], data[3], Boolean.parseBoolean(data[4]), position, Integer.parseInt(data[8]), Float.parseFloat(data[9]));
	            	 }
	            	 else outToClient.writeBytes("Command "+data[0]+" with wrong number of arguments");
	            	 break;
	             case "addSourceAndPlay":
	            	 float[] position = {Float.parseFloat(data[3]), Float.parseFloat(data[4]), Float.parseFloat(data[5])};
            		 this.mySoundSystem.addSourceAndPlay(data[1], data[2], position);
            		 break;
	             case "addSourceLeft": this.mySoundSystem.addSourceLeft(data[1], data[2], Float.parseFloat(data[3])); break;
	             case "addSourceRight": this.mySoundSystem.addSourceRight(data[1], data[2], Float.parseFloat(data[3])); break;
	             case "addSourceFront": this.mySoundSystem.addSourceFront(data[1], data[2], Float.parseFloat(data[3])); break;
	             case "addSourceBack": this.mySoundSystem.addSourceBack(data[1], data[2], Float.parseFloat(data[3])); break;
	             case "addSourceLeftAndPlay": this.mySoundSystem.addSourceLeftAndPlay(data[1], data[2], Float.parseFloat(data[3])); break;
	             case "addSourceRightAndPlay": this.mySoundSystem.addSourceRightAndPlay(data[1], data[2], Float.parseFloat(data[3])); break;
	             case "addSourceFrontAndPlay": this.mySoundSystem.addSourceFrontAndPlay(data[1], data[2], Float.parseFloat(data[3])); break;
	             case "addSourceBackAndPlay": this.mySoundSystem.addSourceBackAndPlay(data[1], data[2], Float.parseFloat(data[3])); break;
	             case "playSource": this.mySoundSystem.playSource(data[1]); break;
	             case "playAllSources":
	            	 if (data.length == 1) this.mySoundSystem.playAllSources();
	            	 else if(data.length == 2) this.mySoundSystem.playAllSources(Long.parseLong(data[1]));
	            	 else outToClient.writeBytes("Command "+data[0]+" with wrong number of arguments");
	            	 break;
	             case "setSourcePosition":
	            	 float[] position1 = {Float.parseFloat(data[2]), Float.parseFloat(data[3]), Float.parseFloat(data[4])};
	            	 this.mySoundSystem.setSourcePosition(data[1], position1);
	            	 break;
	             case "stopExistentSource": this.mySoundSystem.stopExistentSource(data[1]); break;
	             case "removeSource": this.mySoundSystem.removeSource(data[1]); break;
	             case "exitSoundSystem": break;
	             case "addSourceByFinger": this.mySoundSystem.addSourceByFinger(data[1], data[2], data[3], data[4], Float.parseFloat(data[5])); break;
	             case "addSourceByFingerAndPlay": this.mySoundSystem.addSourceByFingerAndPlay(data[1], data[2], data[3], data[4], Float.parseFloat(data[5])); break;
	             case "addSourceByAngle": this.mySoundSystem.addSourceByAngle(data[1], data[2], Float.parseFloat(data[3]), Float.parseFloat(data[5])); break;
	             case "addSourceByAngleAndPlay": this.mySoundSystem.addSourceByAngleAndPlay(data[1], data[2], Float.parseFloat(data[3]), Float.parseFloat(data[5])); break;
	             default: outToClient.writeBytes("Command "+data[0]+" not recognized"); break;
             }

             if (data[0].equals("exitSoundSystem"))
             {
            	 this.mySoundSystem.exitSoundSystem();
                 break;
             }
            }
			socketReader.close(); 

		} catch (IOException e) {
			System.out.println("PrintStream Error");
		}

		try {
			socket.close();
		} catch (IOException e) {
			System.out.println("Failed to close, oddly...");
		}
	}

}
