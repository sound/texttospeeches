package sound_sockets;

import sound_experiments.SoundSystemAPI;
import table_communication.Client;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class SoundSockets {
	ServerSocket socket;
	int port;
	
	SoundSystemAPI mySoundSystem;
	Client client;
	
	public SoundSockets(int port)
	{
		
		this.port = port;
		this.mySoundSystem = new SoundSystemAPI();
		
		client = new Client(mySoundSystem);
		Thread thread = new Thread(client);
		thread.start();
		
		try {
			this.socket = new ServerSocket(port);
			System.out.println("Bound to port: " + port);
		} catch (IOException e) {
			System.out.println("Cannot bind to port: " + port);
			System.exit(0);
		}
		
		
	}
	
	public void acceptInstructions()
	{
		while (true) {
			try {
				Socket clientSocket = this.socket.accept();
				System.out.println("New Client: "+clientSocket.getInetAddress().toString());
				(new Thread(new ClientHandler(clientSocket, this.mySoundSystem))).start();
			} catch (IOException e) {
				System.out.println("Failed to accept client");
			}
		}
	}

	public static void main(String[] args) {

		new SoundSockets(7999).acceptInstructions();
	}
}