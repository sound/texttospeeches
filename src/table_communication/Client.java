package table_communication;

import java.io.IOException;
import java.util.List;

import sound_experiments.SoundSystemAPI;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Client implements Runnable{

	private AmqpClient amqpClient;
	private ObjectMapper mapper;
	private boolean finished;
	private SoundSystemAPI mySoundSystem;

	private void init() throws Exception {
		amqpClient = new AmqpClient("tia", "localhost", new String[]{"app.finish"});
		amqpClient.connect();
		mapper = new ObjectMapper();

	}

	public Client(SoundSystemAPI mySoundSystem) {
		this.mySoundSystem = mySoundSystem;
	}
	
	private void mainloop() throws JsonParseException, JsonMappingException, IOException {
		while (!finished) {

			object sound;

			List<AmqpClient.Message> messages = amqpClient.receiveMessages();
			if (!messages.isEmpty()) {
				// finish if received app.finish
				sound = messages.get(0).getBody(mapper, object.class);

				switch (sound.getFuncao()) {

				case "addSourceAndPlay":
					float[] position = {sound.getX(), sound.getY(), sound.getZ()};
					this.mySoundSystem.addSourceAndPlay(sound.getName(), sound.getFilename(), position);
					break;

				case "addSourceLeftAndPlay": this.mySoundSystem.addSourceLeftAndPlay(sound.getName(), sound.getFilename(), sound.getX()); break;
				case "addSourceRightAndPlay": this.mySoundSystem.addSourceRightAndPlay(sound.getName(), sound.getFilename(), sound.getX()); break;	
				case "addSourceFrontAndPlay": this.mySoundSystem.addSourceFrontAndPlay(sound.getName(), sound.getFilename(), sound.getX()); break;
	            case "addSourceByFingerAndPlay": this.mySoundSystem.addSourceByFingerAndPlay(sound.getName(), sound.getFilename(), sound.getHand(), sound.getFinger(), sound.getX()); break;
			}
		}
	}

}

public void sendMessage(){
	try {
		amqpClient.sendMessage(new AmqpClient.Message(mapper, "coisas", "kinect.handPosition"));
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}		
}

@Override
public void run() {
	try {
		init();
		sendMessage();
		mainloop();
		cleanup();
	} catch (Exception e) {
		e.printStackTrace();
	}


}

private void cleanup() throws Exception {
	amqpClient.disconnect();
}

}
